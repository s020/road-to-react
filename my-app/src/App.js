import * as React from 'react';

const App = () => {
  console.log('app renders')
  const stories = [
    {
      title: 'React',
      url: 'https://reactjs.org/',
      author: 'Jordan Walke',
      num_comments: 3,
      points: 4,
      objectID: 0,
    },
    {
      title: 'Redux',
      url: 'https://redux.js.org/',
      author: 'Dan Abramov, Andrew Clark',
      num_comments: 2,
      points: 5,
      objectID: 1,
    }
  ];

  const [searchTerm, setSearchTerm] = React.useState('React');
  const handleSearch = event => {
    setSearchTerm(event.target.value);
  };
  const searchedStories = stories.filter(x => {
    return x.title.toLowerCase().includes(searchTerm.toLowerCase());
  });
  return (
    <div>
      <h1>My Hacker Stories</h1>
      <Search onSearch={handleSearch} term={searchTerm}/>
      <hr />
      <List list={searchedStories} />
    </div>
  );
};

const Search = (props) => {
  const {onSearch, term} = props;
  return (
    <div>
      <label htmlFor="search">Search: </label>
      <input id="search" type="text" value={term} onChange={onSearch}/>
    </div>
  );
}

const List = ({list}) => (
  <ul>
    {list.map((item) =>
      <Item key={item.objectID} item={item} />
    )}
  </ul>
);

const Item = ({item}) => {
 return (
    <li>
      <span>
        <a href={item.url}>{item.title}</a>
      </span>
      <span> {item.author} </span>
      <span> {item.num_comments} </span>
      <span> {item.points} </span>
    </li>
  );}

export default App;